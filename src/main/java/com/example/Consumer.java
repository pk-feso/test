package com.example;

class Consumer {



    void consume(SyncMap map, int wait) {
        while (true) {
            String first = map.first();
            if(first != null) {
                System.out.println("Smallest: " + first);
            }
            try {
                Thread.sleep(wait);
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted ", e);
            }
        }
    }

}


