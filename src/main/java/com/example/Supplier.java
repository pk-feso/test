package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Supplier {




    public void supply(SyncMap map)  {
        Parser parser = new Parser();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String readLine;
            try {
                readLine = reader.readLine();
                if(readLine == null) {
                    continue;
                }
                int parse = parser.parse(readLine);
                map.put(parse, readLine);
            } catch (NullPointerException e) {
                System.out.println("Error during parse");
            } catch (IOException e) {
                System.out.println("Error during read");
            }
        }
    }


}
