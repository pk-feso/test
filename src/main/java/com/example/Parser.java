package com.example;


import java.util.HashMap;
import java.util.Map;

public class Parser {

    public final Map<String, Integer> values;

    public Parser() {
        values = new HashMap<>();

        values.put("one", 1);
        values.put("two", 2);
        values.put("three", 3);
        values.put("four", 4);
        values.put("five", 5);
        values.put("six", 6);
        values.put("seven", 7);
        values.put("eight", 8);
        values.put("nine", 9);
        values.put("ten", 10);

        values.put("eleven", 11);
        values.put("twelve", 12);
        values.put("thirteen", 13);
        values.put("fourteen", 14);
        values.put("fifteen", 15);
        values.put("sixteen", 16);
        values.put("seventeen", 17);
        values.put("eighteen", 18);
        values.put("nineteen", 19);

        values.put("twenty", 20);
        values.put("thirty", 30);
        values.put("forty", 40);
        values.put("fifty", 50);
        values.put("sixty", 60);
        values.put("seventy", 70);
        values.put("eighty", 80);
        values.put("ninety", 90);

        values.put("hundred", 100);
        values.put("thousand", 1000);


    }

    public int parse(String readLine) {

        String[] split = readLine.split(" ");
        int k = 0;
        int result = 0;
        for (int i = split.length - 1; i >= 0; i--) {


            if (values.get(split[i]).equals(100)) {
                k = 100;
                continue;
            } else if (values.get(split[i]).equals(1000)) {
                k = 1000;
                continue;
            }

            if (k != 0) {
                result += values.get(split[i]) * k;
            } else {
                result += values.get(split[i]);
            }

            k = 0;



        }

      return result;
    }


}
