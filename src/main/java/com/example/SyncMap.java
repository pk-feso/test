package com.example;

import java.util.Map;
import java.util.TreeMap;

public class SyncMap {

    private final TreeMap<Integer, String> map = new TreeMap<>();


    public synchronized String put(Integer key, String value) {
        return map.put(key, value);
    }


    public synchronized String first() {
        Map.Entry<Integer, String> entry = map.firstEntry();
        if(entry == null) {
            return null;
        }
        String value = entry.getValue();
        map.remove(entry.getKey());
        return value;
    }
}
