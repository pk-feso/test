package com.example;


public class Main {

    public static void main(String[] args) {
        start(5000);
    }

    public static void start(int main) {
        final SyncMap map = new SyncMap();
        new Thread(() -> {
            new Consumer().consume(map, main);
        }).start();
        new Thread(() -> {
                new Supplier().supply(map);
        }).start();
    }

}
